<div>
    {{-- A good traveler has no fixed plans and is not intent upon arriving. --}}
    <h1>
        Cursos Becarios DGTIC
    </h1>
    <div class="row">
        <div class="col-sm-3">
            @include("livewire.$view")
        </div>
    </div>
</div>
