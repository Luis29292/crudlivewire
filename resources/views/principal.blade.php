@extends('layout')
@section('content')
    <div class="content">
        <h1>Cursos DGTIC</h1>
        @livewire('curso-component')
    </div>
@endsection